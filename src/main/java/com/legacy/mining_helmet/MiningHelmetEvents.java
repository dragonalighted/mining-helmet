package com.legacy.mining_helmet;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class MiningHelmetEvents
{
	@SubscribeEvent
	public static void onEntityJoinWorld(EntityJoinWorldEvent event)
	{
		Entity entity = event.getEntity();
		World world = event.getWorld();
		if (entity.getType() == EntityType.ZOMBIE && MiningHelmetConfig.zombieHelmetSpawnChance() > 0)
		{
			if (entity.getY() < world.getSeaLevel() && world.getRandom().nextInt(MiningHelmetConfig.zombieHelmetSpawnChance()) == 0)
			{
				entity.setItemSlot(EquipmentSlotType.HEAD, new ItemStack(MiningHelmetRegistry.MINING_HELMET));
				entity.setItemSlot(EquipmentSlotType.MAINHAND, new ItemStack(Items.IRON_PICKAXE));
			}
		}
	}
}
