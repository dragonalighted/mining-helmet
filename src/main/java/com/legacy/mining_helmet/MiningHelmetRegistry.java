package com.legacy.mining_helmet;

import com.legacy.mining_helmet.item.HelmetArmorMaterial;
import com.legacy.mining_helmet.item.MiningHelmetItem;

import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class MiningHelmetRegistry
{
	public static final Item MINING_HELMET = new MiningHelmetItem(HelmetArmorMaterial.MINING, EquipmentSlotType.HEAD, new Item.Properties().tab(ItemGroup.TAB_COMBAT));

	@SubscribeEvent
	public static void onRegisterItems(Register<Item> event)
	{
		register(event.getRegistry(), "mining_helmet", MINING_HELMET);
	}

	private static <T extends IForgeRegistryEntry<T>> void register(IForgeRegistry<T> registry, String name, T object)
	{
		object.setRegistryName(MiningHelmetMod.locate(name));
		registry.register(object);
	}
}