package com.legacy.mining_helmet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.legacy.mining_helmet.client.MiningHelmetClient;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(MiningHelmetMod.MODID)
public class MiningHelmetMod
{
	// Original 1.1.0 rewrite by LaDestitute
	// Rewritten once more for 1.2.0 by Silver_David

	public static final Logger LOGGER = LogManager.getLogger();
	public static final String MODID = "mining_helmet";

	public MiningHelmetMod()
	{
		ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, MiningHelmetConfig.SERVER_SPEC);

		DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> MiningHelmetClient.init());

		MinecraftForge.EVENT_BUS.register(MiningHelmetEvents.class);
		FMLJavaModLoadingContext.get().getModEventBus().register(MiningHelmetRegistry.class);
	}

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MiningHelmetMod.MODID, name);
	}
}
