package com.legacy.mining_helmet.client;

import com.google.common.collect.ImmutableList;
import com.legacy.mining_helmet.MiningHelmetConfig;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.LivingEntity;

public class MiningHelmetModel<T extends LivingEntity> extends BipedModel<T>
{
	public ModelRenderer base;
	public ModelRenderer lid;
	public ModelRenderer light;

	public MiningHelmetModel()
	{
		super(1.0F);

		this.texWidth = 64;
		this.texHeight = 64;

		float offsetAmount = !MiningHelmetConfig.helmetCoversFace() ? 2 : 1;
		this.lid = new ModelRenderer(this, 0, 16);
		this.lid.setPos(0.0F, 0.0F, 0.0F);
		this.lid.addBox(-6.0F, -4.0F - offsetAmount, -6.0F, 12, 2, 12, 0.0F);
		this.base = new ModelRenderer(this, 0, 0);
		this.base.setPos(0.0F, 0.0F, 0.0F);
		this.base.addBox(-4.0F, -8.0F - offsetAmount, -4.0F, 8, 8, 8, 1.0F);
		this.light = new ModelRenderer(this, 32, 0);
		this.light.setPos(0.0F, 0.0F, 0.0F);
		this.light.addBox(-3.0F, -10.0F - offsetAmount, -7.0F, 6, 6, 3, 0.0F);
	}

	@Override
	protected Iterable<ModelRenderer> headParts()
	{
		// TODO SUPER HACKY FIX UNTIL FORGE CAN FIX setRotationAngles
		float offset = this.head.y;

		this.lid.copyFrom(this.head);
		this.base.copyFrom(this.head);
		this.light.copyFrom(this.head);

		this.lid.y = offset;
		this.base.y = offset;
		this.light.y = offset;

		return ImmutableList.of(this.lid, this.base, this.light);
	}

	@Override
	protected Iterable<ModelRenderer> bodyParts()
	{
		return ImmutableList.of();
	}

	@Override
	public void setupAnim(T entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		super.setupAnim(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
	}

	public void setRotateAngle(ModelRenderer ModelRenderer, float x, float y, float z)
	{
		ModelRenderer.xRot = x;
		ModelRenderer.yRot = y;
		ModelRenderer.zRot = z;
	}
}
