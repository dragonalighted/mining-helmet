package com.legacy.mining_helmet.client;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.TickEvent.ClientTickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

@OnlyIn(Dist.CLIENT)
public class MiningHelmetClientEvents
{
	@SubscribeEvent
	public static void clientTick(final ClientTickEvent event)
	{
		if (event.phase == TickEvent.Phase.START)
			DynamicLightingManager.tick();
	}
}
