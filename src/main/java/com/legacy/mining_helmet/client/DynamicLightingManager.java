package com.legacy.mining_helmet.client;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.legacy.mining_helmet.MiningHelmetConfig;
import com.legacy.mining_helmet.MiningHelmetMod;
import com.legacy.mining_helmet.MiningHelmetRegistry;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * Before trying to use this code, please review our licence.
 * 
 * https://moddinglegacy.com/ML-General-Terms/
 * 
 * @author David
 *
 */
@OnlyIn(Dist.CLIENT)
public class DynamicLightingManager
{
	private static final Minecraft MC = Minecraft.getInstance();
	public static final Map<BlockPos, LightData> SOURCES = new ConcurrentHashMap<>();

	public static void tick()
	{
		if (MC.player != null && MC.level != null && MC.player.tickCount % MiningHelmetConfig.lightRefreshRate() == 0)
		{
			SOURCES.forEach((blockPos, data) -> data.shouldStay = false);

			MC.level.getEntitiesOfClass(LivingEntity.class, MC.player.getBoundingBox().inflate(MiningHelmetConfig.maxVisibleDistance()), DynamicLightingManager::shouldGlow).forEach(e -> SOURCES.put(e.blockPosition().above((int) e.getEyeHeight()), new LightData()));

			if (!SOURCES.isEmpty())
			{
				SOURCES.forEach((blockPos, data) -> MC.level.getChunkSource().getLightEngine().checkBlock(blockPos));
				SOURCES.entrySet().removeIf(entry -> !entry.getValue().shouldStay);
			}
		}
	}

	public static boolean shouldGlow(LivingEntity entity)
	{
		if (entity.getItemBySlot(EquipmentSlotType.HEAD).getItem() == MiningHelmetRegistry.MINING_HELMET)
		{
			boolean visible = MiningHelmetConfig.seeThroughWalls();
			if (!visible)
			{
				Vector3d playerPos = new Vector3d(MC.player.getX(), MC.player.getEyeY(), MC.player.getZ());
				Vector3d entityPos = new Vector3d(entity.getX(), entity.getEyeY(), entity.getZ());
				visible = MC.player.level.clip(new RayTraceContext(playerPos, entityPos, RayTraceContext.BlockMode.VISUAL, RayTraceContext.FluidMode.NONE, entity)).getType() == RayTraceResult.Type.MISS;

				if (!visible && MC.player.distanceTo(entity) < 24)
					visible = true;
			}
			return visible;
		}
		return false;
	}

	public static void cleanUp()
	{
		if (SOURCES.size() > 0 && MC.level != null)
		{
			MiningHelmetMod.LOGGER.info(String.format("Cleaning up light data for %s light sources", SOURCES.size()));
			SOURCES.forEach((blockPos, data) ->
			{
				data.shouldStay = false;
				MC.level.getChunkSource().getLightEngine().checkBlock(blockPos);
			});
			SOURCES.clear();
		}
	}

	public static class LightData
	{
		public boolean shouldStay = true;
	}
}